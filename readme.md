# congatec mfgdump tool

This tool is provided to read and print out the content of the congatec manufacturing data
in a human readable way.

# license

The tool can be licensed either under GPLv2 or MIT.
Choose the one that suits your purpose.

# building the tool

The tool is written in C++ and requires an appropriate compiler/toolchain installed.
It is intended to be compiled on a x86 cross development machine. Both variants, x86 and ARM, are
compiled during the make process and are available the respective subdirectory after the build.

Ensure prior building the correct setting of the cross compile environment.
Depending on the type of toolchain/architecture, extra flags for the compiler are required.
They can be specified with the CPPEXTRAFLAGS variable in the makefile (an example is given in
the Makefile for building the tool for i.MX6 with hard-float ABI).

The proper build relies on the correct definition of the SDKTARGETSYSROOT variable, which is set when
sourcing the Yocto cross compile build environment.

In order to build the tool:
1. initialize cross compile environment
   - e.g. in case of Yocto, install the SDK, then source the environment via "source environment-setup..."
2. execute "make" in the root of the mfgdump tool

In case of build errors, check the settings of the CPPEXTRAFLAGS variable in the makefile.

# usage

Copy the tool to the taget system and execute it with the manufacturing partition (usually /dev/mtd2) as parameter, e.g.

```
mfgdump -r /dev/mtd2
```

Copyright (c) 2024, congatec GmbH
