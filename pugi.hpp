/* SPDX-License-Identifier: GPL-2.0+ OR MIT */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 *
 * Author: Michael Schanz <michael.schanz@congatec.com>
 *
 */
namespace pugi
{
	typedef char char_t;
}
