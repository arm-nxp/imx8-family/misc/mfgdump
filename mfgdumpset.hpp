/* SPDX-License-Identifier: GPL-2.0+ OR MIT */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 *
 * Author: Michael Schanz <michael.schanz@congatec.com>
 *
 */

#ifndef MFGDUMPSET_HPP
#define MFGDUMPSET_HPP

#include <list>
#include <map>
#include "mfgitem.hpp"

namespace Mfg
{

  const unsigned char SizeCheckSummed = 0x31;
  const int MaxSectionSize = 32;
  const int DevicePathLength = 256;
  const int FilePathLength = 256;
};

struct mfgdata
{
  unsigned char tsize;		// total size in bytes
  unsigned char ckcnt;		// size of checksummed part in bytes
  unsigned char cksum;		// checksum corrected byte
  unsigned char serial[6];	// decimal serial number, packed BCD
  unsigned char pn[16];		// part number, right justified, ASCII
  unsigned char ean[7];		// ean code, pcked BCD
  unsigned char hwrev[2];	// hardware revision, 1st byte: major, 2nd byte: minor
  unsigned char mfgdate[3];	// manufacturing date, packed BCD, 1st byte: day, 2nd byte month, 3rd byte year
  unsigned char repdate[3];	// last repair   date, packed BCD, 1st byte: day, 2nd byte month, 3rd byte year
  unsigned char manufact[1];	// manufacturer id
  unsigned char project[4];	// project id
  unsigned char repcount[1];	// repair counter
  unsigned char spare[12];	// reserved
  unsigned char calib[1];	// calibration
  unsigned char bootcount[3];	// boot counter, little endian
  unsigned char runtime[2];	// runtime in hours, little endian
};

class MfgDumpSet
{

  std::list < MfgItem * >items;
  mfgdata data;

public:
    MfgDumpSet ();
  unsigned char getCheckSum (void);

  void hexDump (void);
  void strDump (void);
  std::map<std::string, std::string> strDumpRtn();

  int presetFromRaw (char *rawfile);
};
#endif
