/* SPDX-License-Identifier: GPL-2.0+ OR MIT */
/*
 * definition of various MfgItem classes
 *
 * MfgItem: base class to declare MfgItem (manufacturing item), provides the routines to setup and store a MfgItem
 * MfgItemASCII: read/write functions for ASCII values
 * MfgItemBCD: read/write functions for BCD values
 * MfgItemDate: read/write functions for date values
 * MfgItemREV: read/write functions for revision values
 * MfgItemVAL: read/write functions for unsigned integer values
 *
 * Copyright (C) 2023-2024 congatec GmbH
 *
 * Author: Michael Schanz <michael.schanz@congatec.com>
 *
 */
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"mfgitem.hpp"

using namespace std;

// class MfgItem
// constructor
MfgItem::MfgItem (const char *xName, unsigned char *xData, int xLen)
{
  len = xLen;
  data = xData;

  if (strlen (xName) < sizeof (name))
    strcpy (name, xName);
}

// class MfgItem
// writes a string based value to the shadow buffer
int
MfgItem::writeShadow (const pugi::char_t * src)
{
  if (strlen (src) >= MAX_VALUE_SIZE)
    return Mfg::Failure;

  strcpy (shadow, src);
  return Mfg::Success;
}

// class MfgItem
// sets up the value from the shadow buffer
int
MfgItem::setupFromShadow (int *changed)
{
  if (strlen (shadow) == 0)
    return Mfg::Success;

  if (write (shadow) != Mfg::Success)
    {
      std::cout << "Error writing shadow value." << std::endl;
      return Mfg::Failure;
    }
  (*changed)++;
  return Mfg::Success;
}

// class MfgItem
// print BCD coded value as string
void
MfgItem::strDump ()
{
  char str[64];

  if (strlen (name) > 7)
    std::cout << name << "\t: ";
  else
    std::cout << name << "\t\t: ";

  if (read (str) != Mfg::Success)
    std::cout << "Error reading value." << std::endl;
  else
    std::cout << str << std::endl;
}

// class MfgItem
// create pair from name and str
std::pair<std::string, std::string>
MfgItem::strDumpRtn ()
{
  char str[64];
  if (read (str) == Mfg::Success)
      return make_pair(string(name), string(str));
  else
      return make_pair(string(name), "");
}

// class MfgItemBCD
// read BCD coded value
int
MfgItemBCD::read (pugi::char_t * dst)
{
  pugi::char_t * dptr = dst + (len * 2);
  unsigned char *sptr = data + len;

  int remain = len * 2;

  *dptr = 0x0;
  dptr--;
  sptr--;
  while (remain > 0)
    {
      if (!(remain % 2))
	{
	  // odd: low nibble
	  *dptr = ((*sptr) & 0x0F) + 0x30;
	}
      else
	{
	  // even: high nibble
	  *dptr = (((*sptr) & 0xF0) >> 4) + 0x30;
	  sptr--;
	}
      dptr--;
      remain--;
    }

  return Mfg::Success;
}

// class MfgItemBCD
// write BCD coded value
int
MfgItemBCD::write (const pugi::char_t * src)
{
  int i;
  const pugi::char_t * sptr = src + strlen (src);	// set pointer to end of string
  unsigned char *dptr = data + len;

  if (strlen (src) > (len * 2))
    return Mfg::Failure;

  memset (data, 0, len);

  i = 0;
  sptr--;			// adjust pointer to very last element of source string
  dptr--;
  while (sptr >= src)
    {
      if (*sptr < '0' || *sptr > '9')
	return Mfg::Failure;

      if (!i)
	{
	  i++;
	  *dptr = (*sptr) - 0x30;
	  sptr--;
	}
      else
	{
	  i--;
	  *dptr |= ((*sptr) - 0x30) << 4;
	  sptr--;
	  dptr--;
	}
    }
  return Mfg::Success;
}

// class MfgItemASCII
// read ASCII coded value
int
MfgItemASCII::read (pugi::char_t * dst)
{
  unsigned char *sptr = data;
  pugi::char_t * dptr = dst;
  int remain = len;

  while (remain)
    {
      if (*sptr)
	{
	  *dptr = *sptr;
	  dptr++;
	}
      sptr++;
      remain--;
    }
  *dptr = 0x0;
  return Mfg::Success;
}

// class MfgItemASCII
// write ASCII coded value
int
MfgItemASCII::write (const pugi::char_t * src)
{
  int remain;
  int i;
  const pugi::char_t * sptr = src + strlen (src);	// set pointer to end of string
  unsigned char *dptr = data + len;

  if (strlen (src) > len)
    return Mfg::Failure;

  memset (data, 0, len);

  i = 0;
  sptr--;			// adjust pointer to very last element of source string
  dptr--;
  while (sptr >= src)
    {
      *dptr = *sptr;
      sptr--;
      dptr--;
    }

  return Mfg::Success;
}

// class MfgItemDATE
// read DATE coded value
int
MfgItemDATE::read (pugi::char_t * dst)
{
  int yearPreset = 20;

  // just BCD date len 3 supported
  if (len != 3)
    return Mfg::Failure;

  if (data[0] == 0 && data[1] == 0 && data[2] == 0)
    yearPreset = 0;

  switch (type)
    {
    case dtType_YY_EU:
      sprintf (dst, "%d%d.%d%d.%d%d",
	       ((data[0] & 0xF0) >> 4), (data[0] & 0xF),
	       ((data[1] & 0xF0) >> 4), (data[1] & 0xF),
	       ((data[2] & 0xF0) >> 4), (data[2] & 0xF));
      break;
    case dtType_YYYY_EU:
      sprintf (dst, "%d%d.%d%d.%02d%d%d",
	       ((data[0] & 0xF0) >> 4), (data[0] & 0xF),
	       ((data[1] & 0xF0) >> 4), (data[1] & 0xF),
	       yearPreset, ((data[2] & 0xF0) >> 4), (data[2] & 0xF));
      break;
    case dtType_YY_US:
      sprintf (dst, "%d%d-%d%d-%d%d",
	       ((data[2] & 0xF0) >> 4), (data[2] & 0xF),
	       ((data[1] & 0xF0) >> 4), (data[1] & 0xF),
	       ((data[0] & 0xF0) >> 4), (data[0] & 0xF));
      break;
    case dtType_YYYY_US:
      sprintf (dst, "%02d%d%d-%d%d-%d%d",
	       yearPreset,
	       ((data[2] & 0xF0) >> 4), (data[2] & 0xF),
	       ((data[1] & 0xF0) >> 4), (data[1] & 0xF),
	       ((data[0] & 0xF0) >> 4), (data[0] & 0xF));
      break;
    }

  return Mfg::Success;
}

// class MfgItemDATE
// write DATE coded value
int
MfgItemDATE::write (const pugi::char_t * src)
{
  // accepted date formats:
  // DD.MM.YY - EU format, 2 digits year
  // DD.MM.YYYY - EU format, 4 digits year
  // YY-MM-DD - US format, 2 digits year
  // YYYY-MM-DD - US format, 4 digits year
  // YY/MM/DD - US format, 2 digits year
  // YYYY/MM/DD - US format, 4 digits year

  pugi::char_t * sptr;
  pugi::char_t tmpdate[11];
  unsigned char ldata[3];
  int y_digits;

  // just BCD date len 3 supported
  if (len != 3)
    return Mfg::Failure;

  // determine format
  if (strlen (src) != 8 && strlen (src) != 10)
    {
      // invalid format
      return Mfg::Failure;
    }

  if (strlen (src) == 8)
    y_digits = 2;
  else
    y_digits = 4;



  if (src[2] == '.' && src[5] == '.')
    {
      // EU format - store day to dst[0], month to dst[1], year to dst[2]

      int tmp;

      strcpy (tmpdate, src);
      tmpdate[2] = 0;
      tmpdate[5] = 0;

      // day must be between 1 and 31
      tmp = ((tmpdate[0] - 0x30) << 4) + (tmpdate[1] - 0x30);
      if (tmp < 0x01 || tmp > 0x31)
	return Mfg::Failure;
      ldata[0] = tmp;

      // month must be between 1 and 12
      tmp = ((tmpdate[3] - 0x30) << 4) + (tmpdate[4] - 0x30);
      if (tmp < 0x01 || tmp > 0x12)
	return Mfg::Failure;
      ldata[1] = tmp;

      if (y_digits == 2)
	ldata[2] = ((tmpdate[6] - 0x30) << 4) + (tmpdate[7] - 0x30);
      else
	ldata[2] = ((tmpdate[8] - 0x30) << 4) + (tmpdate[9] - 0x30);

      memcpy (data, ldata, 3);
      return Mfg::Success;
    }

  if ((src[y_digits] == '-' && src[y_digits + 3] == '-')
      || (src[y_digits] == '/' && src[y_digits + 3] == '/'))
    {
      // US format - store day to dst[0], month to dst[1], year to dst[2]

      int tmp;

      strcpy (tmpdate, src);
      tmpdate[y_digits] = 0;
      tmpdate[y_digits + 3] = 0;


      // day must be between 1 and 31
      tmp =
	((tmpdate[y_digits + 4] - 0x30) << 4) + (tmpdate[y_digits + 5] -
						 0x30);
      if (tmp < 0x01 || tmp > 0x31)
	return Mfg::Failure;
      ldata[0] = tmp;

      // month must be between 1 and 12
      tmp =
	((tmpdate[y_digits + 1] - 0x30) << 4) + (tmpdate[y_digits + 2] -
						 0x30);
      if (tmp < 0x01 || tmp > 0x12)
	return Mfg::Failure;
      ldata[1] = tmp;

      ldata[2] =
	((tmpdate[y_digits - 2] - 0x30) << 4) + (tmpdate[y_digits - 1] -
						 0x30);
      memcpy (data, ldata, 3);
      return Mfg::Success;
    }

  return Mfg::Failure;
}

// class MfgItemDATE
// set date type
int
MfgItemDATE::setType (int xType)
{
  type = xType;
  return Mfg::Success;
}

// class MfgItemREV
// read revision coded value
int
MfgItemREV::read (pugi::char_t * dst)
{
  sprintf (dst, "%c.%c", data[0], data[1]);
  return Mfg::Success;
}

// class MfgItemREV
// write revision coded value
int
MfgItemREV::write (const pugi::char_t * src)
{
  int index;

  if (strlen (src) == 3)
    index = 2;
  else
    {
      if (strlen (src) == 2)
	index = 1;
      else
	return Mfg::Failure;
    }

  if (src[0] < 'A' || src[0] > 'Z')
    return Mfg::Failure;

  if (index == 2 && src[1] != '.')
    return Mfg::Failure;

  if (src[index] < '0' || src[index] > '9')
    return Mfg::Failure;

  data[0] = src[0];
  data[1] = src[index];
  return Mfg::Success;
}

// class MfgItemVAL
// read value coded value
int
MfgItemVAL::read (pugi::char_t * dst)
{
  unsigned char *ptr = data;
  unsigned int value;
  int remain = len;

  value = 0;
  while (remain)
    {
      value += (*ptr) & 0xFF;
      ptr++;
      remain--;
      if (remain)
	value <<= 8;
    }

  sprintf (dst, "%d", value);

  return Mfg::Success;
}

// class MfgItemVAL
// write value coded value
int
MfgItemVAL::write (const pugi::char_t * src)
{
  unsigned int i;
  unsigned char *dptr = data + len - 1;

  i = atoi (src);

  if (i > ((1 << (len * 8)) - 1))
    return Mfg::Failure;

  while (dptr >= data)
    {
      unsigned char c = (i % 256);
      *dptr = c;
      i = (i >> 8);
      dptr--;
    }
  return Mfg::Success;
}
