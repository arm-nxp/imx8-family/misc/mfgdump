/* SPDX-License-Identifier: GPL-2.0+ OR MIT */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 *
 * Author: Michael Schanz <michael.schanz@congatec.com>
 *
 */

#ifndef MFGITEM_HPP
#define MFGITEM_HPP

#include "pugi.hpp"
#include <string>
#include <utility>

const int dtType_YY_EU = 0;
const int dtType_YYYY_EU = 1;
const int dtType_YY_US = 2;
const int dtType_YYYY_US = 3;

const int MAX_VALUE_SIZE = 64;

namespace Mfg
{

  const int Success = 1;
  const int Failure = 0;

};

class MfgItem
{

protected:

  int len;
  unsigned char *data;
  char shadow[MAX_VALUE_SIZE];

public:

  char name[16];

    MfgItem (const char *xName, unsigned char *xData, int xLen);

  virtual int read (pugi::char_t * dst) = 0;
  virtual int write (const pugi::char_t * src) = 0;
  virtual int setType (int xType)
  {
    return 0;
  };
  int writeShadow (const pugi::char_t * src);
  int setupFromShadow (int *changed);
  void strDump ();
  std::pair<std::string, std::string>  strDumpRtn ();
};

class MfgItemBCD:public MfgItem
{

public:

  MfgItemBCD (const char *xName, unsigned char *xData,
	      int xLen):MfgItem (xName, xData, xLen)
  {
  };

  int read (pugi::char_t * dst);
  int write (const pugi::char_t * src);
};

class MfgItemASCII:public MfgItem
{

public:

  MfgItemASCII (const char *xName, unsigned char *xData,
		int xLen):MfgItem (xName, xData, xLen)
  {
  };

  int read (pugi::char_t * dst);
  int write (const pugi::char_t * src);
};

class MfgItemREV:public MfgItem
{

public:

  MfgItemREV (const char *xName, unsigned char *xData,
	      int xLen):MfgItem (xName, xData, xLen)
  {
  };

  int read (pugi::char_t * dst);
  int write (const pugi::char_t * src);
};

class MfgItemVAL:public MfgItem
{

public:

  MfgItemVAL (const char *xName, unsigned char *xData,
	      int xLen):MfgItem (xName, xData, xLen)
  {
  };

  int read (pugi::char_t * dst);
  int write (const pugi::char_t * src);
};

class MfgItemDATE:public MfgItem
{

  int type;

public:

    MfgItemDATE (const char *xName, unsigned char *xData, int xLen,
		 int xType):MfgItem (xName, xData, xLen), type (xType)
  {
  };

  int read (pugi::char_t * dst);
  int write (const pugi::char_t * src);
  int setType (int xType);
};
#endif
