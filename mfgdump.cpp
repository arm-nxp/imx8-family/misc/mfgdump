/* SPDX-License-Identifier: GPL-2.0+ OR MIT */
/*
 * main program
 *
 * Copyright (C) 2023-2024 congatec GmbH
 *
 * Author: Michael Schanz <michael.schanz@congatec.com>
 *
 */
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string.h>
#include<time.h>
#include"mfgdumpset.hpp"

using namespace std;

/* flag definitions */
const int FLAG_READ = 0x0001;
const int FLAG_VERBOSE = 0x8000;

unsigned int flags;

const char *version = "V1.1 (c) 2023 congatec GmbH";

void
usage ()
{
  cout << "congatec ARM Manufacturing Data Dump Tool" << endl;
  cout << version << endl;
  cout << endl;
  cout << "Usage:  mfgdump -r device [-v]" << endl;
  cout << endl;
}

int
main (int argc, char *argv[])
{
  int c;
  MfgDumpSet *set = new MfgDumpSet ();
  char device[Mfg::DevicePathLength];

  flags = 0;

  // parse options
  while ((c = getopt (argc, argv, "hvr:")) != -1)
    {
      switch (c)
	{
	case 'h':
	  usage ();
	  delete set;
	  exit (-1);
	case 'v':
	  flags |= FLAG_VERBOSE;
	  break;
	case 'r':
	  if (strlen (optarg) >= Mfg::DevicePathLength)
	    {
	      cout << "Devicename exceeds maximum size." << endl;
	      delete set;
	      exit (-1);
	    }
	  else
	    strcpy (device, optarg);
	  flags |= FLAG_READ;
	  break;
	}
    }

  if (flags & (FLAG_READ))
    set->presetFromRaw (device);
  else
    {
      usage ();
      delete set;
      exit (-1);
    }

  if (flags & (FLAG_VERBOSE))
    set->hexDump ();

  set->strDump ();

  delete set;
  exit (0);
}
