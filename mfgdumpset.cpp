/* SPDX-License-Identifier: GPL-2.0+ OR MIT */
/*
 * definition of a mfgDumpSet
 * A MfgDumpSet is a collection of several MfgItem objects arranged in the same way as they appear in the mfg data area.
 *
 * Copyright (C) 2023-2024 congatec GmbH
 *
 * Author: Michael Schanz <michael.schanz@congatec.com>
 *
 */
#include<iostream>
#include<list>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"mfgdumpset.hpp"

using namespace std;

MfgDumpSet::MfgDumpSet ()
{
  MfgItem *item;

  memset (&data, 0, sizeof (mfgdata));

  item =
    (MfgItem *) new MfgItemBCD ("serial", data.serial, sizeof (data.serial));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemASCII ("partnumber", data.pn, sizeof (data.pn));
  items.push_back (item);

  item = (MfgItem *) new MfgItemBCD ("ean", data.ean, sizeof (data.ean));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemREV ("hwrev", data.hwrev, sizeof (data.hwrev));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemDATE ("mfgdate", data.mfgdate,
				 sizeof (data.mfgdate), dtType_YYYY_US);
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemDATE ("repdate", data.repdate,
				 sizeof (data.repdate), dtType_YYYY_US);
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemVAL ("manufacturer", data.manufact,
				sizeof (data.manufact));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemASCII ("project", data.project,
				  sizeof (data.project));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemVAL ("repcount", data.repcount,
				sizeof (data.repcount));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemVAL ("calibration", data.calib,
				sizeof (data.calib));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemVAL ("bootcount", data.bootcount,
				sizeof (data.bootcount));
  items.push_back (item);

  item =
    (MfgItem *) new MfgItemVAL ("runtime", data.runtime,
				sizeof (data.runtime));
  items.push_back (item);
}

// determine checksum corretion byte
unsigned char
MfgDumpSet::getCheckSum (void)
{
  int tmpSum = 0;
  int remain = data.ckcnt;
  unsigned char *ptr = (unsigned char *) &data;
  unsigned char tmpStore = data.cksum;

  data.cksum = 0;
  while (remain)
    {
      tmpSum += *ptr;
      ptr++;
      remain--;
    }

  data.cksum = tmpStore;
  return (256 - (tmpSum % 256));
}

// prints out hexadecimal values of data structure
void
MfgDumpSet::hexDump (void)
{
  int i;
  char hexval[5];
  unsigned char *ptr = (unsigned char *) &data;

  cout << endl << "data map:" << endl;

  for (i = 0; i < sizeof (mfgdata); i++)
    {
      sprintf (hexval, "0x%02x", *ptr);
      cout << hexval << " ";
      if (!((i + 1) % 16))
	cout << endl;
      ptr++;
    }
  cout << endl;
}

// prints out string values of data structure
void
MfgDumpSet::strDump (void)
{
  list < MfgItem * >::iterator it;
  char hexval[5];
  char buf[32];
  unsigned long val;

#if 0
  sprintf (hexval, "0x%02x", data.ckcnt);
  cout << "checksum count\t: " << hexval << endl;

  sprintf (hexval, "0x%02x", data.cksum);
  cout << "checksum\t: " << hexval << endl;
#endif

  for (it = items.begin (); it != items.end (); ++it)
    {
      MfgItem *item = *it;
      item->strDump ();
    }

  if (getCheckSum () != data.cksum)
    cout << "INVALID CHECKSUM!!!" << endl;
}

// presets the mfgdata struct from a raw file
int
MfgDumpSet::presetFromRaw (char *rawfile)
{
  FILE *ifile;
  size_t size;

  if ((ifile = fopen (rawfile, "rb")) == NULL)
    {
      cerr << "Error opening " << rawfile << endl;
      return Mfg::Failure;
    }

  size = fread (&data, sizeof (mfgdata), 1, ifile);
  fclose (ifile);

  if (size != 1)
    {
      cerr << "Error reading data (items read: " << size << " )" << endl;
      return Mfg::Failure;
    }

  return Mfg::Success;
}

// returns string values of data structure
std::map<std::string, std::string> MfgDumpSet::strDumpRtn(){
  std::map<std::string, std::string> rtnMap;
  for (list < MfgItem * >::iterator it = items.begin (); it != items.end (); ++it){
    MfgItem *item = *it;
    rtnMap.insert(item->strDumpRtn());
  }

  if (getCheckSum () != data.cksum){
      //cout << "INVALID CHECKSUM!!!" << endl;
      std::map<std::string, std::string> temp;
      return temp;
  }
  return rtnMap;
}
