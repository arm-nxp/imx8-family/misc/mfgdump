#
# Makefile mfgdump

CC=g++
CC-ARM=$(CROSS_COMPILE)g++

# uncomment to enable debug
#CPPFLAGS+=-g

# extra flags for ARMv7 hard float build (e.g. i.MX6/Yocto Kirkstone BSP)
#CPPEXTRAFLAGS+=-mfloat-abi=hard -march=armv7-a+fp

.PHONY: clean chksysroot

all: chksysroot mfgdump
objects: mfgdump
headers: pugi.hpp mfgitem.hpp mfgdumpset.hpp

chksysroot:
ifndef SDKTARGETSYSROOT
	$(error variable SDKTARGETSYSROOT is not defined)
endif

mfgdump: mfgdump.cpp mfgdumpset.cpp mfgitem.cpp $(headers)
	$(CC) $(CPPFLAGS) $< mfgdumpset.cpp mfgitem.cpp -o x86/$@ 
	$(CC-ARM) $(CPPFLAGS) $(CPPEXTRAFLAGS) --sysroot=$(SDKTARGETSYSROOT) $< mfgdumpset.cpp mfgitem.cpp -o ARM/$@ $(LDFLAGS)

clean:
	rm -rf *.o *~ x86/mfg* ARM/mfg*
